#!/bin/bash -v

# Get GPG key for grafana, add repository
curl https://packagecloud.io/gpg.key | sudo apt-key add -
add-apt-repository "deb https://packagecloud.io/grafana/stable/debian/ stretch main"

# Update packages
apt-get update -y

# Install packages
apt-get install -y grafana libapache2-mod-wsgi apache2 graphite-web unzip default-jre

# Install graphite-carbon
DEBIAN_FRONTEND=noninteractive apt-get install -y graphite-carbon

# Configure apache to host graphite
echo 'CARBON_CACHE_ENABLED=true' > /etc/default/graphite-carbon
chown _graphite /var/lib/graphite; sudo -u _graphite graphite-manage syncdb --noinput
cp /usr/share/graphite-web/apache2-graphite.conf /etc/apache2/sites-available/

# Download and unzip gatling
cd /tmp || exit 1
wget -nv "https://repo1.maven.org/maven2/io/gatling/highcharts/gatling-charts-highcharts-bundle/2.3.1/gatling-charts-highcharts-bundle-2.3.1-bundle.zip"
unzip "gatling-charts-highcharts-bundle-2.3.1-bundle.zip" -d /opt
cp /vagrant/configs/gatling.conf /opt/gatling-charts-highcharts-bundle-2.3.1/conf/gatling.conf

# Start weather app
cp -r /vagrant/webapp /opt
echo "java -jar /opt/webapp/*.jar >/vagrant/app-start.log" | at now

# Create SSL certs
openssl req -new -newkey rsa:4096 -days 3650 -nodes -x509 -subj '/C=CC/ST=State/L=City/O=Organization/CN=localhost' -keyout /etc/ssl/private/apache-selfsigned.key -out /etc/ssl/certs/apache-selfsigned.crt

# Set up apache for weather app
cp /vagrant/configs/weatherapp.conf /etc/apache2/sites-available/
sed -i '/Listen 80/a Listen 8888' /etc/apache2/ports.conf
# Do not enable ssl or Gatling cannot access
# a2enmod ssl
a2enmod proxy_http
a2dissite 000-default; a2ensite apache2-graphite; a2ensite weatherapp

# Start & enable services
systemctl start grafana-server carbon-cache
systemctl restart apache2
systemctl enable apache2 grafana-server carbon-cache
