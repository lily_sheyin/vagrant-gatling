## Gatling Task

### Prerequisites
* VirtualBox
* Vagrant
* X-Quartz

### Info & Setup
* A Vagrant configuration deploying the java weather app fronted by a secured apache proxy, carbon-cache with graphite hosted on apache, grafana, and gatling.
* Start with `vagrant init` then `vagrant up`. Init logs are saved to this directory.
* Gatling and Weather App files are located in `/opt`
* Graphite dashboard - `localhost:8080`
* Grafana dashboard - `localhost:3030`
* Weather App - `https://localhost:4443`
